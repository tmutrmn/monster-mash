"use strict";

var TILE_WIDTH = 112;

var PlatformGroup = function (game) {
	Phaser.Group.call(this, game);
	this.game = game;
	this.physicsBodyType = Phaser.Physics.ARCADE;
};



PlatformGroup.prototype = Object.create(Phaser.Group.prototype);
PlatformGroup.prototype.constructor = PlatformGroup;

var tile1 = null, tile2 = null;
var len = 0, m = 0, n = 0, tileN = 0;
var stepDown = false;
var enemyX = 0, enemyY = 0;

PlatformGroup.prototype.generatePlatform = function(x, y, first) { 
		if (y < 140) y = 140;
		else if (y > 320) y = 320;
		
		stepDown = true;
		if (first) { 
			len = 16;
			stepDown = false;
		}	else len = this.game.rnd.integerInRange(2, 16);
		
		m = 0; // this.game.rnd.integerInRange(0,1);
		n = 0;
		if (len > 8) {
			n = this.game.rnd.integerInRange(2,len-4);
			//n = 3;
		} else n = -1;
		//console.log(len, m , n);
		
		for (var i=0; i<len; i++) {
			tileN = this.game.rnd.integerInRange(1,4);
			if ( (m==0) && (n==i) && (y < 300) && (!first) ) {	//   
				tileN=6;
				stepDown = false;
			}
			/*if ( stepDown && (i == Math.round(len/3)) ) { 
				tileN=6;
				stepDown = false;
			}*/
			if (first && i==0) {
				tileN = this.game.rnd.integerInRange(1,4); 
			} else if (!first && i==0) {
				tileN = 0; 
			}	else if (i==len-1) {
				tileN = 5;
			}
			
			if (tileN!=6) {
				tile1 = this.create(x + i*TILE_WIDTH, y-40, 'tiles', tileN);
				tile1.anchor.setTo(0.0, 0.0);
				this.game.physics.enable(tile1, Phaser.Physics.ARCADE);
				tile1.body.immovable = true;
				tile1.body.gravity.y = -GRAVITY;
				tile1.body.velocity.x = BasicGame.speed;
				tile1.body.setSize(tile1.body.sourceWidth, tile1.body.sourceHeight-40, 0, 40);
				if (tileN==0) {
					tile1.body.setSize(86, tile1.body.sourceHeight-40, 42, 40);
				}
				if (tileN==5) {
					tile1.body.setSize(84, tile1.body.sourceHeight-40, 0, 40);
				}
				tile1.smoothed = false;
			}
			else if (tileN==6) {
				tile1 = this.create(x + i*TILE_WIDTH, y-40, 'tiles', 6);
				tile1.anchor.setTo(0.0, 0.0);
				this.game.physics.enable(tile1, Phaser.Physics.ARCADE);
				tile1.body.immovable = true;
				tile1.body.gravity.y = -GRAVITY;
				tile1.body.velocity.x = BasicGame.speed;
				tile1.body.setSize(tile1.body.sourceWidth, tile1.body.sourceHeight-40, 0, 40);
				tile1.smoothed = false;
				y = y + 110;
			}
					
		}
		/*if (-dist.body.x / 10 < 1000) gap = this.game.rnd.integerInRange(1,2); 
		else if (-dist.body.x / 10 < 2000) gap = this.game.rnd.integerInRange(1,3); 
		else if (-dist.body.x / 10 < 3000) gap = this.game.rnd.integerInRange(1,4); 
		else */
		this.gap = this.game.rnd.integerInRange(2,7); 

		tile2 = this.create(x + len * TILE_WIDTH, 640, 'empty');
		tile2.scale.setTo(this.gap, 1.0);
		this.game.physics.enable(tile2, Phaser.Physics.ARCADE);
		tile2.body.immovable = true;
		tile2.body.gravity.y = -GRAVITY;
		tile2.body.velocity.x = BasicGame.speed;
		tile2.smoothed = false;
		
		
		if (!first) {
			if (len/2<enemiesTotal) var enemiesNum = len/2;
			else  var enemiesNum = enemiesTotal;
			var rand = 0, prevRand = 0;
			for (i=0; i<enemiesNum; i++) {
				prevRand = rand;
				rand = this.game.rnd.integerInRange(1, len-1);
				if (rand == prevRand) rand++;
				enemyX = x + rand * 128; 		// this.game.rnd.integerInRange(x, x+len*128);		 // this.game.rnd.integerInRange(600, 1280);		//
				// enemyX = this.game.rnd.integerInRange(600, 1280);
				enemyY = y-200;
				enemies.push(new Enemy(i, this.game, enemyX, enemyY));
			}
		}
		
		
		x = x + len * TILE_WIDTH + this.gap * 32;
		
		y = y;
		return [x, y]
};