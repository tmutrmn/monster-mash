"use strict";

BasicGame.Game = function (game) {
	//	When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:
	this.game;		//	a reference to the currently running game
	this.add;		//	used to add sprites, text, groups, etc
	this.camera;	//	a reference to the game camera
	this.cache;		//	the game cache
	this.input;		//	the global input manager (you can access this.input.keyboard, this.input.mouse, as well from it)
	this.load;		//	for preloading assets
	this.math;		//	lots of useful common math operations
	this.sound;		//	the sound manager - add a sound, play one, set-up markers, etc
	this.stage;		//	the game stage
	this.time;		//	the clock
	this.tweens;	//	the tween manager
	this.world;		//	the game world
	this.particles;	//	the particle manager
	this.physics;	//	the physics manager
	this.rnd;		//	the repeatable random number generator
	//	You can use any of these from any function within this State.
	//	But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.

	
	this.paused = false;
	this.gameover = false;
	this.restartButton = null;
	
	var floor = null;
	var player = null;
	
	
	var platforms = null;
	var platformsBgr = null;
	var gaps = null;
	var lastSprite = null;
	var firstSprite = null;
	
	var enemiesLayer = null;
	
	
};

BasicGame.speed = -200; // -150;	

BasicGame.cursors = null,
BasicGame.jumpButton = null,
BasicGame.shootButton = null,
BasicGame.pauseButton = null;


BasicGame.walkingSound = null,
BasicGame.jumpSound = null,
BasicGame.crushSound = null,
BasicGame.ouchSound = null,
BasicGame.fallingSound = null,
BasicGame.coinSound = null;
BasicGame.coinDropSound = null;
BasicGame.shotgunFireSound = null,
BasicGame.shotgunReloadSound = null;

BasicGame.playerBulletsLayer = null;


BasicGame.coinsLayer = null;
BasicGame.coins = [];


var player = null;
var onGround = false;


var playerLayer = null,
		enemiesLayer = null,
		enemies = [];

var enemiesTotal = 2;

var skyBgr = null,
		bgrFaraway = null,
		bgrNearby = null,
		bgrClouds = null;
		
var platforms = null,
		platformsTop = null,
		gaps = null,
		platformLayer = null;
		



var playerRunAnim = null,
		playerShootAnim = null,
		shot = null,		
		shotgunShotAnim = null,
		shootTimer = 0;
		
var jumpTimer = 0;

var timer = 0;
var startTime = null;
var playerJumped = false;

var dist, distance = 0;
var GRAVITY = 200;
var addEnemiesTimer = 5000;

var emitter = null;

		
var pointsRetroFont = null,
    pointsTextImage = null,
		levelRetroFont = null,
		levelTextImage = null,
		pauseRetroFont = null,
		pauseTextImage = null,
		gameoverRetroFont = null,
		gameoverTextImage = null;


var gap = 0; 
	
var lastGap = 0;
var lastX = 0, lastY = 0;
var lastCoords = [];


var tiles1 = null, tiles2 = null;	
var len = 0, lastLen = 0, newLen = 0;
		
var firstSprite = [], lastSprite = [];
		
		
var back_emitter = null,
		mid_emitter = null,
		front_emitter = null;







BasicGame.Game.prototype = {

	create: function () {
		this.game.time.advancedTiming = true;
		this.world.setBounds(0, 0, 960*10, 540);
		this.camera.y = 540;
		this.camera.bounds = new Phaser.Rectangle(0,0,960, 540);
		
		this.physics.startSystem(Phaser.Physics.ARCADE);
    this.time.desiredFps = 60;
		this.physics.arcade.gravity.y = GRAVITY;
		
		skyBgr = this.add.tileSprite(0, 0, 960*10, 540, 'skyBgr');
		skyBgr.smoothed = false;
		bgrFaraway = this.add.tileSprite(0, 0, this.world.bounds.width, this.world.bounds.height, 'bgrFaraway');
		bgrFaraway.smoothed = false;
		//bgrFaraway.scale.setTo(1.46875, 1.46875);
		bgrClouds = this.add.tileSprite(0, 0, this.world.bounds.width, this.world.bounds.height, 'bgrClouds');
		bgrClouds.smoothed = false;
		//bgrClouds.scale.setTo(1.46875, 1.46875);
		bgrNearby = this.add.tileSprite(0, 0, this.world.bounds.width, this.world.bounds.height, 'bgrNearby');
		bgrNearby.smoothed = false;
		//bgrNearby.scale.setTo(1.46875, 1.46875);

		
		dist = this.add.sprite(100, 0, 'empty');
		dist.anchor.setTo(0.5, 0.0);
		this.physics.enable(dist, Phaser.Physics.ARCADE);
		dist.body.collideWorldBounds = false;
		dist.body.gravity.y = -GRAVITY;
		dist.body.velocity.x  = BasicGame.speed;
		

		
		playerLayer = this.game.add.group();
		player = new Player(this.game, this.game.width / 4, 80);
		
		platforms = [];
		for (var j=0; j<3; j++) {
			platforms[j] = new PlatformGroup(this.game);
		}
		lastX = 0; lastY = 200;
		
		enemiesLayer = this.game.add.physicsGroup();		// this.game.add.group();		// order!
		enemies = [];
		enemiesTotal = 2;
		

		BasicGame.coinsLayer = this.game.add.group();		// this.game.add.group();		// order!
		BasicGame.coins = [];

		
		lastCoords = platforms[0].generatePlatform(lastX, 240, true);
		lastCoords = platforms[1].generatePlatform(lastCoords[0], lastCoords[1] + 60, false);
		lastCoords = platforms[2].generatePlatform(lastCoords[0], lastCoords[1] + this.game.rnd.integerInRange(-5, 5) * 10, false);
		
		this.game.world.bringToTop(playerLayer);


		

		
		
		jumpTimer = 0;
		onGround = false;
		playerJumped = false;
		
		
		BasicGame.playerBulletsLayer = this.game.add.physicsGroup();

		
		BasicGame.fallingSound = this.add.audio('falling', 0.5, true);
		BasicGame.walkingSound = this.add.audio('walking', 0.5, true);
		BasicGame.jumpSound = this.add.audio('jump', 0.33, false);
		BasicGame.crushSound = this.add.audio('crush', 0.5, false);
		BasicGame.ouchSound = this.add.audio('ouch', 0.5, false);
		BasicGame.coinSound = this.add.audio('coin', 0.5, false);
		BasicGame.coinDropSound = this.add.audio('coinDrop', 0.5, false);
		BasicGame.shotgunFireSound = this.add.audio('shotgunFire', 0.5, false);
		BasicGame.shotgunReloadSound = this.add.audio('shotgunReload', 0.5, false);
		BasicGame.shotgunReloadSound.onStop.add(player.reloaded);
				
		this.camera.follow(player.player, Phaser.Camera.FOLLOW_LOCKON);
		
		// cursors = this.input.keyboard.createCursorKeys();
		
		BasicGame.jumpButton = this.input.keyboard.addKey(Phaser.Keyboard.ENTER);
		BasicGame.shootButton = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
		BasicGame.pauseButton = this.input.keyboard.addKey(Phaser.Keyboard.P);
		BasicGame.pauseButton.onDown.add(this.pauseGame, this);
		
		startTime = this.time.now;
		distance = 0;
		
		
		pointsRetroFont = this.game.add.retroFont('bitmapFont', 18, 24, Phaser.RetroFont.TEXT_SET11, 13, 0, 0);
    pointsRetroFont.text = "COINS: 0 ";
    pointsTextImage = this.game.add.image(this.game.width / 10 * 7, 32, pointsRetroFont);
    pointsTextImage.anchor.set(0.5,1.0);
		
		levelRetroFont = this.game.add.retroFont('bitmapFont', 18, 24, Phaser.RetroFont.TEXT_SET11, 13, 0, 0);
		levelRetroFont.text = "LEVEL: " + this.level + "";
		levelTextImage = this.game.add.image(this.game.width-20, 32, levelRetroFont);
		levelTextImage.anchor.set(1.0,1.0);

		gameoverRetroFont = this.game.add.retroFont('bitmapFont', 18, 24, Phaser.RetroFont.TEXT_SET11, 13, 0, 0);
		gameoverRetroFont.text = "GAME OVER";
		gameoverTextImage = this.game.add.image(this.game.width/2, -100, gameoverRetroFont);
		gameoverTextImage.anchor.set(0.5,0.5);
		gameoverTextImage.scale.set(1.5,1.5);
		this.gameover = false;
		
		pauseRetroFont = this.game.add.retroFont('bitmapFont', 18, 24, Phaser.RetroFont.TEXT_SET11, 13, 0, 0);
		pauseRetroFont.text = "GAME PAUSED";
		pauseTextImage = this.game.add.image(this.game.width/2, -100, pauseRetroFont);
		pauseTextImage.anchor.set(0.5,0.5);
		pauseTextImage.scale.set(1.5,1.5);
		
		
		this.restartButton = this.add.button(this.game.width / 2, -100, 'restartButton', this.restart, this);		// this.game.height/6 * 5
		this.restartButton.anchor.setTo(0.5, 0.5);
		this.restartButton.scale.setTo(0.5, 0.5);
				
		
		
		
		
		emitter = this.add.emitter(0, 0, 100);
		emitter.makeParticles('coin', 0, 2, true);
		emitter.gravity = -200;
		
/*	
		back_emitter = this.add.emitter(this.game.width / 2, -32, 600);
		back_emitter.makeParticles('snowflakes', [0, 1, 2, 3, 4, 5]);
		back_emitter.maxParticleScale = 0.6;	back_emitter.minParticleScale = 0.2;
		back_emitter.minRotation = 0;					back_emitter.maxRotation = 40;
		back_emitter.setYSpeed(20, 100);			back_emitter.gravity = 0;
		back_emitter.width = this.game.width * 1.5;
		

		mid_emitter = this.add.emitter(this.game.width / 2, -32, 250);
		mid_emitter.makeParticles('snowflakes', [0, 1, 2, 3, 4, 5]);
		mid_emitter.maxParticleScale = 1.2;		mid_emitter.minParticleScale = 0.8;
		mid_emitter.minRotation = 0;					mid_emitter.maxRotation = 40;
		mid_emitter.setYSpeed(50, 150);				mid_emitter.gravity = 0;
		mid_emitter.width = this.game.width * 1.5;
		

		front_emitter = this.add.emitter(this.game.width / 2, -32, 50);
		front_emitter.makeParticles('snowflakes_large', [0, 1, 2, 3, 4, 5]);
		front_emitter.maxParticleScale = 1;		front_emitter.minParticleScale = 0.5;
		front_emitter.minRotation = 0;				front_emitter.maxRotation = 40;
		front_emitter.setYSpeed(100, 200);		front_emitter.gravity = 0;
		front_emitter.width = this.game.width * 1.5;


    back_emitter.start(false, 14000, 20);
		mid_emitter.start(false, 12000, 40);
		front_emitter.start(false, 6000, 1000);
*/		

		this.time.events.loop(1000, function() { if (player.alive) BasicGame.speed--; }, this);
		this.time.events.loop(10000, function() {
			if (enemiesTotal<8) {
				enemiesTotal = 3; //Math.round(-dist.body.x/2000);
				//console.log('enemiesTotal: ' + enemiesTotal);
			}
		}, this);
	},
	
	

	killPlayer: function(pointer) {
	},
	
	shoot: function() {
		shotgunFireSound.play('', 0, 0.33, false, true);
		//gunLoaded = false;
		gunLoaded--;
		if (gunLoaded == 0)	{
			shotgunFireSound.onStop.addOnce(this.reload);
		}
	},
	reload: function() {
		shotgunReloadSound.play('', 0, 0.5, false, false);
	},
	reloaded: function() {
		//gunLoaded = true;
		gunLoaded = 2;
	},
	
	
	
	
	
	updateCounter: function () {
		//counter++;
		//text.setText('Counter: ' + counter);
		//BasicGame.speed--;
	},
	
	
	generateNewPlatform: function(x, y) {
		//return [x, y]
	},
		
	
	pauseGame: function()
	{
		this.input.keyboard.reset(false);
		this.game.paused = !this.game.paused;
		
		if (this.game.paused) {
			pauseTextImage.y = this.game.height/2 - 40;
		} else if (!this.game.paused) {
			pauseTextImage.y = -100;
		}
	},
		
		
	update: function () 
	{
		if (!this.game.paused) 
		{
			if (player.player.y > 540) player.alive = false;
			
			for (var i = 0; i < enemies.length; i++)
			{
				if (enemies[i].alive) {
					enemies[i].update();
				} else if (!enemies[i].alive) {
					enemies[i].npc.destroy();
					enemies.splice(i, 1);
				}
			}
			
			for (var i = 0; i < BasicGame.coins.length; i++)
			{
				if (BasicGame.coins[i].alive) {
					BasicGame.coins[i].update();
				} else if (!BasicGame.coins[i].alive) {
					BasicGame.coins[i].coin.kill();
					BasicGame.coins.splice(i, 1);
				}
			}
			
			player.update();

			
			var xx = 0;
			for (var i=0; i<3; i++) {
				firstSprite[i] = platforms[i].getAt(0);
				lastSprite[i] = platforms[i].getAt(platforms[i].total-1);
				platforms[i].forEach(function(item) {
					item.body.velocity.x = BasicGame.speed;
				});
				/*platformsTop[i].forEach(function(item) {
					item.body.velocity.x = BasicGame.speed;
				});*/
				if (lastSprite[i].x > xx) xx = Math.floor(lastSprite[i].x + lastSprite[i].width);
			}
		
			for (var i=0; i<3; i++) { 
				if (lastSprite[i].x < 0) {
					var num = i;
					platforms[num].removeAll();
					//platformsTop[num].removeAll();
					if (lastCoords[1] > 320) lastCoords = platforms[num].generatePlatform(xx, lastCoords[1] - this.game.rnd.integerInRange(1, 5) * 10, false);
					else lastCoords = platforms[num].generatePlatform(xx, lastCoords[1] - this.game.rnd.integerInRange(-5, 5) * 10, false);
				}
			}
			
			
			distance = parseInt(-dist.body.x / 32);
		
			levelRetroFont.text = "" + distance + "m";
			pointsRetroFont.text = "COINS: " + BasicGame.score + " ";
			
		
			bgrFaraway.tilePosition.x += BasicGame.speed/1400;
			bgrClouds.tilePosition.x += BasicGame.speed/1000;
			bgrNearby.tilePosition.x += BasicGame.speed/600;
		
			// console.log(player.alive);
			
			if (!player.alive) {
				dist.body.velocity.x = 0;
				if (!this.gameover) {
					gameoverTextImage.y = this.game.height/2 - 40;
				}
				this.restartButton.y = this.game.height/7 * 4;
				this.gameover = true;
			}
			
				
		}		// player alive && ! paused

		if (this.input.keyboard.isDown(Phaser.Keyboard.Q)) {
			this.game.destroy();
		}
	},
	
	
	pauseUpdate: function() {
		this.input.keyboard.reset(false);
		if (Math.round(this.game.time.now/100) % 10 == 0)
			pauseRetroFont.text = "GAME PAUSED";
		else if (Math.round(this.game.time.now/100) % 10 == 5)
			pauseRetroFont.text = " ";
		
		if (this.game.paused) {
			pauseTextImage.y = this.game.height/2 - 40;
		}
	},
	
	restart: function() {
		BasicGame.score = 0;
		BasicGame.speed = -200;
		this.game.state.start('Game');
	},

	quitGame: function (pointer) {
		this.time.removeAll();
		this.state.start('MainMenu');
	},
	
		
	preRender: function () {
	},
	
	render: function () {	
		/*
		this.game.debug.cameraInfo(this.camera, 20, 120);
		this.game.debug.text(BasicGame.speed, 900, 80);
		
		this.game.debug.text(this.game.time.fps + ' fps', 10, 60);
		this.game.debug.text(this.game.time.suggestedFps + ' fps', 10, 80);
		*/
		// this.game.debug.body(player);
	},

	quitGame: function (pointer) {
		BasicGame.score = 0;
		this.game.destroy();
		this.state.start('MainMenu');
	}


};
