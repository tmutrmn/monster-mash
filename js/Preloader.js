
BasicGame.Preloader = function (game) {

	this.background = null;
	this.preloadBar = null;

	this.ready = false;
	this.tweenReady = false;
	this.tween = null;
};

BasicGame.Preloader.prototype = {

	preload: function () {
		//this.background = this.add.sprite(0, 0, 'preloaderBackground');
		var bgr = this.background = this.add.sprite(this.game.width / 2, this.game.height / 4, 'preloaderBackground');		//	These are the assets we loaded in Boot.js
		bgr.anchor.setTo(0.5, 0.5);
		bgr.scale.setTo(1.0, 1.0);
		this.preloadBar = this.add.sprite(this.game.width / 2 - 200, 460, 'preloaderBar');
		this.load.setPreloadSprite(this.preloadBar);
		this.preloadBar.cropEnabled = true;

		// this.load.image('titlepage', 'assets/title.jpg');
		this.load.image('playButton', 'assets/play-button.png');
		this.load.image('quitButton', 'assets/quit-button.png');
		this.load.image('restartButton', 'assets/restart-button.png');
		this.load.image('titleScreen', 'assets/title-screen1.jpg');
		// this.load.atlas('playButton', 'assets/play_button.png', 'assets/play_button.json');
		this.load.audio('titleMusic', ['assets/audio/title-music.mp3']);
		//this.load.bitmapFont('desyrel', 'assets/fonts/desyrel.png', 'assets/fonts/desyrel.xml');
		this.load.image('bitmapFont', 'assets/fonts/bitmap-font_03.png');
		
		
		this.load.audio('shotgunFire', ['assets/audio/shotgun-fire.mp3']);
		this.load.audio('shotgunReload', ['assets/audio/shotgun-reload2.mp3']);
		this.load.audio('walking', ['assets/audio/walking.mp3']);
		this.load.audio('jump', ['assets/audio/jump1.mp3']);
		this.load.audio('crush', ['assets/audio/crush3.mp3']);
		this.load.audio('ouch', ['assets/audio/ouch2.mp3']);
		this.load.audio('falling', ['assets/audio/falling.mp3']);
		this.load.audio('coin', ['assets/audio/coin.mp3']);
		this.load.audio('coinDrop', ['assets/audio/coin-drop.mp3']);
		
		
		this.load.image('skyBgr', 'assets/background.png');
		this.load.image('bgrFaraway', 'assets/bgr_faraway.png');
		this.load.image('bgrNearby', 'assets/bgr-nearby3.png');
		this.load.image('bgrClouds', 'assets/bgr_clouds.png');
		
		
		
		// this.load.spritesheet('player', 'assets/run.png', 34, 56, 2);
		this.load.spritesheet('player', 'assets/barry.png', 58, 74, 24);
		this.load.spritesheet('shotgun-shot', 'assets/shotgun-shot.png', 116, 57, 6);
		this.load.spritesheet('scientist', 'assets/scientist.png', 64, 64, 9);
		//this.load.spritesheet('tiles', 'assets/wall-tiles_winter.png', 128, 512, 8);
		this.load.spritesheet('tiles', 'assets/wall-tiles_winter_112x440.png', 112, 440, 8);
		this.load.spritesheet('heart', 'assets/heart3.png', 32, 32, 2);
		this.load.spritesheet('coin', 'assets/coin.png', 32, 32, 8);
		this.load.spritesheet('sparkle', 'assets/sparkle.png', 80, 80, 14);
		this.load.image('empty', 'assets/empty.png');
		this.load.image('empty-tile', 'assets/empty-tile.png');
		
		this.load.spritesheet('snowflakes', 'assets/snowflakes.png', 17, 17);
    this.load.spritesheet('snowflakes_large', 'assets/snowflakes_large.png', 64, 64);
		
		this.load.script('Coin.js', 'js/Coin.js');
		this.load.script('Enemy.js', 'js/Enemy.js');
		this.load.script('Platform.js', 'js/Platform.js');
		this.load.script('Player.js', 'js/Player.js');
	},

	create: function () {
		this.preloadBar.cropEnabled = false;
		// this.time.events.add(Phaser.Timer.SECOND * 1, this.fadePicture, this);
		this.tween = this.game.add.tween(this.background).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, false);
		this.tween.onComplete.add(this.onComplete, this);
	},
	
	fadePicture: function() {
		/*this.tween = this.game.add.tween(this.background).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, false);
		this.tween.onComplete.add(this.onComplete, this);*/
	},
	onComplete: function () {
    this.tweenReady = true;
	}, 

	update: function () {
		
		if (this.cache.isSoundDecoded('titleMusic') && this.ready == false)
		{
			this.tween.start()
			if (this.tweenReady) {
				this.ready = true;
				this.state.start('MainMenu');
			}
		}
	}

};
