"use strict";

var Enemy = function (index, game, x, y)
{
	this.game = game;
	this.health = 5;
	this.value = game.rnd.integerInRange(1, 4);
	this.speed = BasicGame.speed;
	this.dir = game.rnd.integerInRange(0, 1);
	this.vel = 0;
	this.fireRate = 1000;
	this.nextFire = 0;
	this.alive = true;
	this.onGround = false;
	
	this.playerHit = false;
	this.hit = false;
	
	
	this.npc = enemiesLayer.create(x, y, 'scientist');
	this.npc.name = index.toString();
	this.npc.anchor.setTo(0.5, 1.0);
	this.npc.body.setSize(this.npc.body.sourceWidth/2, this.npc.body.sourceHeight, 0, 0);
	//this.npc.scale.setTo(1.2, 1.2);
	this.npc.scale.setTo(0.9, 0.9);
	
	this.game.physics.enable(this.npc , Phaser.Physics.ARCADE);
	this.npc.body.linearDamping = 1;
	this.npc.body.bounce.y = 0.0;
	this.npc.body.collideWorldBounds = false;
	if (this.dir == 0) {
		this.npc.scale.x = +this.npc.scale.x;; // not flipped
		this.vel = 50; // this.game.rnd.integerInRange(50, 100);
	}	else if (this.dir == 1) {
		this.npc.scale.x = -this.npc.scale.x;;	//flipped
		this.vel = -50; //this.game.rnd.integerInRange(-50, -100);
	}
	this.npc.body.velocity.x = 0;
	this.npc.body.gravity.y = GRAVITY;
	
	this.enemyDieAnim = this.npc.animations.add('die', [4,5,6,7,8], 10, false);
	this.enemyDieAnim.onComplete.add(this.enemyKill, this);

	this.enemyWalkAnim = this.npc.animations.add('walk', [0,1,2,3], 10, true);
	this.npc.animations.play('walk');
};

Enemy.prototype.damage = function() 
{
	this.health -= 1;
	if (this.health <= 0)
	{
		this.alive = false;
		return true;
	}
	return false;
}

Enemy.prototype.createCoins = function(x,y,vel,val)
{
	for (var i=0; i<val; i++) {
		BasicGame.coins.push(new Coin(this.game, x, y, vel));
	}
};

Enemy.prototype.update = function() 
{
	this.onGround = false;
	this.game.physics.arcade.collide(this.npc, [platforms[0], platforms[1], platforms[2]], this.enemyCollisionHandler, null, this);		// 

	if (this.dir == 1) {		// && platform.frame == 6 && platform.key == 'tiles') {
		
		if (this.game.physics.arcade.overlap(this.npc, [platforms[0], platforms[1], platforms[2]], function(enemy, platform) { 
			if (platform.frame == 6 && platform.key == 'tiles') {
				this.dir = 0;
				this.npc.scale.x = -this.npc.scale.x; // flip
				this.vel = this.game.rnd.integerInRange(50, 100);
				this.npc.body.velocity.x = this.vel;
			}
		}, null, this)) console.log('stairs - turn around!');
		
	}
	
	if (this.npc.inCamera && this.onGround) {
	// if ( (this.npc.x > 0) && (this.npc.x < 1280) && (this.npc.y > 0) && (this.npc.y < 752) && this.onGround ) {
		this.npc.body.velocity.x = this.vel;
	}	else if (this.npc.inCamera && !this.onGround) {
		if (this.dir == 0) this.npc.body.velocity.x = this.speed + this.vel;
		else if (this.dir == 1) this.npc.body.velocity.x = this.speed + this.vel; //speed + this.vel;		// LEFT!
	}
	
	this.game.physics.arcade.overlap(this.npc, player.player, this.enemyPlayerOverlap, null, this);
	
	/*
	if (this.game.physics.arcade.overlap(this.npc, shot, function() { 
			if (!this.npc.playerHit) {
				this.createCoins(this.npc.x, this.npc.y, this.speed + this.vel, this.value);
				this.npc.playerHit = true;
				this.npc.animations.play('die');
				crushSound.play('', 0, 0.5, false, true); 
			}
		}, null, this)) console.log('hit enemy with a shotgun');
	*/
	
	if (this.game.physics.arcade.overlap(this.npc, BasicGame.playerBulletsLayer, function() { 
			if (!this.npc.playerHit) {
				this.createCoins(this.npc.x, this.npc.y, this.speed + this.vel, this.value);
				this.npc.playerHit = true;
				this.npc.animations.play('die');
				BasicGame.crushSound.play('', 0, 0.5, false, true); 
			}
		}, null, this)) console.log('hit enemy with a shotgun');
		
	if (this.npc.x < 0 || this.npc.y > this.game.camera.bounds.height) this.alive = false;
};

Enemy.prototype.enemyKill = function()
{
	this.health = this.health-5;	
	if (this.health <= 0) this.alive = false;
};

Enemy.prototype.enemyPlayerOverlap = function(enemy, playerSprite) 
{
	var angle = Phaser.Math.radToDeg(this.game.physics.arcade.angleBetween(playerSprite, enemy));
	if ( (angle < 120) && (angle > 60) && (playerSprite.body.velocity.y > 0) && (playerSprite.body.y < enemy.body.y) && (!enemy.playerHit) ) {
		enemy.playerHit = true;
		this.createCoins(this.npc.x, this.npc.y, this.speed + this.vel, this.value);
		BasicGame.crushSound.play('', 0, 0.5, false, false);
		this.npc.animations.play('die');
		if (BasicGame.jumpButton.isDown) {		//player is on the ground, so he is allowed to start a jump
			BasicGame.walkingSound.stop();
			player.jumpTimer = 1;
			playerSprite.body.velocity.y = -400;
			BasicGame.jumpSound.play('', 0, 0.5, false, true);
		} else if (BasicGame.jumpButton.isDown && (player.jumpTimer != 0)) { //player is no longer on the ground, but is still holding the jump key
			if (player.jumpTimer > 30) {		// player has been holding jump for over 30 frames, it's time to stop him
				player.jumpTimer = 0;
			} else { // player is allowed to jump higher (not yet 30 frames of jumping)
				player.jumpTimer++;
				playerSprite.body.velocity.y = -400;
			}
		} else if (player.jumpTimer != 0) { //reset jumptimer since the player is no longer holding the jump key
			player.jumpTimer = 0;
		} else {
			playerSprite.body.velocity.y = -200;
		}
	} else if (!enemy.playerHit) {
		enemy.playerHit = true;
		playerSprite.damage(1);	// player.health--;
		player.hearts[playerSprite.health].frame = 1;
		playerSprite.animations.play('hit', 15, false);
		BasicGame.ouchSound.play('', 0, 0.5, false, false);
	}
};

Enemy.prototype.enemyCollisionHandler = function (enemy, platform) 
{
	if (this.dir == 1) {		// && platform.frame == 6 && platform.key == 'tiles') {
		
		if (this.game.physics.arcade.overlap(enemy, platform, function() { 
			if (platform.frame == 6 && platform.key == 'tiles') {
				this.dir = 0;
				this.npc.scale.x = -enemy.scale.x; // flip
				this.vel = this.game.rnd.integerInRange(50, 100);
				enemy.body.velocity.x = this.vel;
			}
		}, null, this)) console.log('stairs - turn around!');
		
	} else if (this.dir == 0 && platform.frame == 6) {
		this.vel = this.game.rnd.integerInRange(50, 100);
		enemy.body.velocity.x = this.vel;
	}

	this.onGround = true;
	if (enemy.y > platform.y+60) { 
		this.onGround = false;
		// if (enemy.x > 0 && enemy.x < 1280 && enemy.y > 0 && enemy.y < 752) fallingSound.play('', 0, 0.5, false, true);
	}
};