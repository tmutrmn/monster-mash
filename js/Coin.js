"use strict";

var Coin = function (game, x, y, vel)
{
	this.game = game;
	this.coin = BasicGame.coinsLayer.create(x, y, 'coin', 0);
	this.alive = true;
	this.game.physics.enable(this.coin, Phaser.Physics.ARCADE);
	this.coin.scale.set(0.75,0.75);
	this.coin.anchor.set(0.5,0.5);
	this.coin.body.gravity.y = GRAVITY;
	this.coin.body.velocity.x = vel + this.game.rnd.integerInRange(-100, 200);
	this.coin.body.velocity.y = this.game.rnd.integerInRange(-200, -400);
	this.coin.body.bounce.x = 0.5;
	this.coin.body.bounce.y = 0.5;
	
	this.coin.animations.add('coin-roll', [0,1,2,3,4,5,6,7], 30, true);
	this.coin.animations.play('coin-roll', 30, true);
	BasicGame.coinDropSound.play('', 0, 0.25, false, true);
};

Coin.prototype.update = function() 
{
	this.game.physics.arcade.collide(this.coin, [platforms[0], platforms[1], platforms[2]], null, null, this);
	this.game.physics.arcade.overlap(this.coin, player.player, this.getCoin, null, this);
	
	if (this.coin.x < 0 || this.coin.y > this.game.camera.bounds.height) { 
		this.alive = false;
	}
};

Coin.prototype.getCoin = function(coin,player)
{
	BasicGame.score++;
	BasicGame.coinSound.play('', 0, 0.5, false, true);
	this.alive = false;
	
	var sparkle = BasicGame.coinsLayer.create(coin.x-32, player.y - player.body.sourceHeight - 16, 'sparkle', 0);		// coin.y-32
	this.game.physics.enable(sparkle, Phaser.Physics.ARCADE);
	sparkle.scale.set(0.85,0.85);
	sparkle.anchor.set(0.5,0.0);
	sparkle.body.gravity.y = -GRAVITY;
	sparkle.body.velocity.x = BasicGame.speed;
	sparkle.body.velocity.y = 0;
	
	sparkle.animations.add('sparkling', [0,1,2,3,4,5,6,7,9,10,11,12,13], 10, false);
	sparkle.animations.play('sparkling', 10, false, true);
	
		this.coin.kill();
};