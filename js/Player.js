"use strict";

var Player = function (game, x, y)
{
	this.game = game;
	this.health = 5;
	this.speed = BasicGame.speed;

	this.fireRate = 1000;
	this.nextFire = 0;
	this.alive = true;
	this.onGround = false;
	this.jumpTimer = 0;
	
	this.gunLoaded = 2;
	this.shootTimer = 0;

	//this.player = this.game.add.sprite(this.game.width / 4, 80, 'player');
	this.player = playerLayer.create(x, y, 'player');
	this.player.anchor.setTo(0.5, 1.0);
	this.player.scale.setTo(0.75,0.75);
	this.game.physics.enable(this.player, Phaser.Physics.ARCADE);
	this.player.body.setSize(50, 72, 0, 0);
	this.player.body.mass = 70.0;
	this.player.health = 5;
	this.player.body.linearDamping = 1;
	this.player.body.bounce.y = 0.0;
  this.player.body.collideWorldBounds = false;
	this.player.body.velocity.x = 0;
	this.player.body.gravity.y = 400;


	this.playerShootAnim = this.player.animations.add('shoot', [7,8,9,10,11], 15, false);
	this.playerShootAnim.onComplete.add(this.playerShootAnimStopped, this);
	this.playerHitAnim = this.player.animations.add('hit', [12,15,12,15,12,15,12,15,14,15, 0,15,1,15,2,15,3,15,4,15,5,15], 15, false);		// , [7,8,9,10,11,11,10,9,8,7], 15, false);
	this.playerHitAnim.onComplete.add(this.playerHitAnimStopped, this);
	this.playerJumpAnim = this.player.animations.add('jump', [16,17,18,19,20,21,22,23], 10, false);
	//this.playerShootAnim.onComplete.add(this.playerShootAnimStopped, this);
	this.playerRunAnim = this.player.animations.add('run', [0,1,2,3,4,5], 15, true);
	this.player.animations.play('run', 15, true);
	
	this.hearts = [];
	for (var i=0; i < this.health; i++) this.hearts[i] = playerLayer.create(20 + i*42, 14, 'heart');
	
	
};


Player.prototype.update = function() 
{
	this.onGround = false;
	this.player.body.velocity.x = 0;
	this.game.physics.arcade.collide(this.player, [platforms[0],platforms[1],platforms[2]], this.collisionHandler, null, this);
	if (this.onGround && !BasicGame.walkingSound.isPlaying) BasicGame.walkingSound.play('', 0, 0.5, false, true);
	
	if ( (this.alive && BasicGame.jumpButton.isDown && this.onGround) || (this.alive && this.game.input.activePointer.isDown && this.game.input.activePointer.position.x > 480 && this.onGround) ) {		//player is on the ground, so he is allowed to start a jump
		console.log('jump!');
		//this.player.animations.stop('run', 0);
		BasicGame.walkingSound.stop();
		this.jumpTimer = 1;
		this.player.body.velocity.y = -400;
		BasicGame.jumpSound.play('', 0, 0.33, false, true);
	} else if (BasicGame.jumpButton.isDown && (this.jumpTimer != 0)) { //player is no longer on the ground, but is still holding the jump key
		if (this.jumpTimer > 30) {		// player has been holding jump for over 30 frames, it's time to stop him
			this.jumpTimer = 0;
		} else { // player is allowed to jump higher (not yet 30 frames of jumping)
			this.jumpTimer++;
			this.player.body.velocity.y = -400;
		}
	} else if (this.jumpTimer != 0) { //reset jumptimer since the player is no longer holding the jump key
		this.jumpTimer = 0;
	}
	
	if (this.jumpTimer > 0) {
		if (this.player.body.velocity.y < 0) {
			// this.player.frame = 16 + Math.ceil(this.jumpTimer/7.5);
			console.log(this.player.body.velocity.y,this.player.frame);
		}
	}
	
	if ( (this.alive) && (this.gunLoaded > 0) && ( (BasicGame.shootButton.isDown) || (this.game.input.activePointer.isDown && this.game.input.activePointer.position.x < 480) )  && (this.game.time.now > this.shootTimer) ) {
		this.shootTimer = this.game.time.now + 250;
		this.player.animations.play('shoot', 15, false);

		shot = BasicGame.playerBulletsLayer.create(this.player.x+29, this.player.y-46, 'shotgun-shot', 0);
		this.game.physics.enable(shot, Phaser.Physics.ARCADE);
		shot.body.setSize(140, 56, 0, 10);
		shot.body.gravity.y = -GRAVITY;
		shot.animations.add('shotgun-shot', [0,1,2,3,4,5], 30, false);
		shot.animations.play('shotgun-shot', 30, false, true);

		this.shoot();
	}
};


Player.prototype.collisionHandler = function (player, platform) 
{
	this.onGround = true;
	if (player.y < platform.y) this.onGround = false;
	/*
	if (this.player.y > platform.y) {
		player.alive = false;
		//dist.body.velocity.x = 0;
		player.onGround = false;
	}
	*/
	player.body.velocity.x = -BasicGame.speed;
};


Player.prototype.shoot = function() {
	BasicGame.shotgunFireSound.play('', 0, 0.33, false, true);
	this.gunLoaded--;
	if (this.gunLoaded == 0)	{
		BasicGame.shotgunFireSound.onStop.addOnce(this.reload);
	}
};

Player.prototype.reload = function() {
	BasicGame.shotgunReloadSound.play('', 0, 0.5, false, false);
};

Player.prototype.reloaded = function() {
	player.gunLoaded = 2;
};
	

Player.prototype.playerHitAnimStopped = function (sprite, animation) {
	console.log('playerHitAnimStopped');
	this.player.animations.play('run');
};
	
Player.prototype.playerShootAnimStopped = function (sprite, animation) {
	this.player.animations.play('run');
};