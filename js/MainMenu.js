
BasicGame.MainMenu = function (game) {

	this.music = null;
	this.playButton = null;

};

BasicGame.MainMenu.prototype = {

	create: function () {
		this.music = this.add.audio('titleMusic');
		this.music.volume = 0.01;
		this.music.play();
		
		this.titlescreen = this.add.sprite(0, 0, 'titleScreen');
		this.titlescreen.alpha = 0;
		this.tween = this.game.add.tween(this.titlescreen).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true);
		
		this.playButton = this.add.button(this.game.width / 2, this.game.height/6 * 5, 'playButton', this.startGame, this);	// , this.startGame, this, 'buttonOver', 'buttonOut', 'buttonOver');
		this.playButton.anchor.setTo(0.5, 0.5);
		this.playButton.scale.setTo(0.5, 0.5);
	},

	update: function () {
		//	Do some nice funky main menu effect here
		if (this.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
			this.startGame();
		}
	},

	startGame: function (pointer) {
		this.music.stop();
		this.state.start('Game');
	}

};
